import '../Model/info_me.dart';
import '../server_APIs/api_auth.dart';
import 'package:flutter/cupertino.dart';

class InfoMeProvider with ChangeNotifier {
  InfoMeModel? infoMeModel;

  bool loadingInfo = false;

  getInfoMe(context) async {
    loadingInfo = true;
    infoMeModel = await ServerApiAuth.infoMe(context);
    loadingInfo = false;
    notifyListeners();
  }
}
