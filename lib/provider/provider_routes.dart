import '../Model/container_model.dart';
import '../server_APIs/api_get_all_routes.dart';

import 'package:flutter/cupertino.dart';

class RoutesProvider with ChangeNotifier {
  // --==-=-==- get all routes -=-=-=-
  ContainersModel? modelAllRoute;
  bool loadingAllRoutes = false;
  List<Data>? data;

  int page = 1;

  getAllRoutes(context) async {
    loadingAllRoutes = true;
    modelAllRoute = await getAllRoutesAPI(context, page: page);
    if (page == 1) {
      data = modelAllRoute!.data;
    } else {
      data!.addAll(modelAllRoute!.data);
    }
    page++;
    loadingAllRoutes = false;
    notifyListeners();
  }

  // -=-=--=  Get Description Route -=-=-=-=
  Data? modelDescriptionRoute;
  bool loadingDescriptionRoute = false;

  getDescriptionRoute(context) async {
    loadingDescriptionRoute = true;
    modelDescriptionRoute = await getDescriptionRoute(context);
    loadingDescriptionRoute = false;
    notifyListeners();
  }
}
