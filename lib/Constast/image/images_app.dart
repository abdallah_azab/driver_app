const String pathImage = "assets/images";

class Images {
  static const String blueRoute = "$pathImage/blueRoute.png";
  static const String carId = "$pathImage/car_id.png";
  static const String carType = "$pathImage/car_type.png";
  static const String circleAvatar = "$pathImage/circelavatar.png";
  static const String greenRoute = "$pathImage/greenRoute.png";
  static const String icon = "$pathImage/icon.png";
  static const String info = "$pathImage/info.png";
  static const String infoGraph = "$pathImage/infograph.png";
  static const String infoGraphGreen = "$pathImage/infographGreen.png";
  static const String logout = "$pathImage/logout.png";
  static const String person = "$pathImage/person.png";
  static const String personDrawer = "$pathImage/person_drawer.png";
  static const String personId = "$pathImage/person_id.png";
  static const String phoneIcon = "$pathImage/phone_icon.png";
  static const String report = "$pathImage/reporte.png";
  static const String reports = "$pathImage/reports.png";
  static const String start = "$pathImage/start.png";
  static const String track = "$pathImage/track.png";
  static const String userIcon = "$pathImage/user_icon.png";
  static const String mapCardIcon = "$pathImage/map_card_icon.png";
  static const String refresh = "$pathImage/refresh.png";
}
