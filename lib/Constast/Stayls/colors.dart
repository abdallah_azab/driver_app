import 'package:flutter/material.dart';

class ColorsApp {
  static Color green = const Color(0xff59AC4C);

  static Color lightOrange = const Color(0xffF6A320);

  static Color defaultColor = const Color(0xff1B588B);

  static Color darkRed = const Color(0xff931c5c);

  static Color darkBlue = const Color(0xff213152);

  static Color blackBlue = const Color(0xff011B43);

  static Color deepOrange = const Color(0xffF66E20);

  static Color lactic = const Color(0xff4DBCE4);

  static Color lightBlue = const Color(0xff1B588B);

  static Color offlight = const Color(0xffD9D9D9);

  static Color tail = const Color(0xff8CE8C0);

  static Color grey = const Color(0xff9A9A9A);

  static Color deepGrey = const Color(0xff707070);

  static Color black = const Color(0xff292929);
}
