import 'dart:convert';
import '../Model/info_me.dart';
import '../Constast/vaiabels.dart';
import '../Helpers/SnackBar/snack_bar.dart';
import '../UI/screens/login/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

// =-=-  header to Api login =--= the API==> ( /auth/token )
var headersLogin = {
  'Content-Type': "application/json",
  'User-Agent': "PostmanRuntime/7.28.4",
  'Accept': "*/*"
};

class ServerApiAuth {
  //  =-=-=-  to Login -=-=-
  static Future<bool> loginApi(
      {required context, required userName, required password}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var url = Uri.parse('$baseUrlAPI/auth/token');
    Map bodyJson = {"username": "$userName", "password": "$password"};

    Response response =
        await http.post(url, body: jsonEncode(bodyJson), headers: headersLogin);
    debugPrint(":::::: statusCode ::::::: " + response.statusCode.toString());

    if (response.statusCode == 200) {
      debugPrint("=-=- Success to Login =-=-");
      Map data = jsonDecode(response.body);
      await preferences.setString("TOKEN_TYPE", data['token_type']);
      await preferences.setString("ACCESS_TOKEN", data['access_token']);
      await preferences.setString("REFRESH_TOKEN", data['refresh_token']);

      debugPrint(" =-=- TOKEN_TYPE =-=-  " +
          preferences.getString("TOKEN_TYPE").toString());
      debugPrint(" =--=-=- TOKEN_ACCESS -=-=-=- " +
          preferences.getString("ACCESS_TOKEN").toString());
    } else {
      debugPrint("::::: failed to Login :::::");
      showSnackBarCustom(text: "failed to Login", context: context);
    }
    return response.statusCode == 200 ? true : false;
  }

  // the code to use ===>    ServerApiAuth.loginApi(context: context, user Name: 'mohamed.hessen@ix-sol.com', password: '1234567890');

  // -=-=-   Log Out  -=-=-=-
  static Future<bool> logOut(context) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    var url = Uri.parse('$baseUrlAPI/logout');
    String? tokenType = preferences.getString("TOKEN_TYPE");
    String? tokenAccess = preferences.getString("ACCESS_TOKEN");

    var headerLogOut = {'Authorization': '$tokenType $tokenAccess'};
    Response response = await http.post(url, headers: headerLogOut);
    debugPrint(":::::: statusCode ::::::: " + response.statusCode.toString());
    if (response.statusCode == 200) {
      debugPrint("=-=- Success to Log Out =-=-");
      // Map data = jsonDecode(response.body);
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => const LoginScreen(),
          ));
    } else {
      debugPrint("::::: failed to Log out :::::");
      showSnackBarCustom(text: "failed to Log Out", context: context);
    }
    return response.statusCode == 200 ? true : false;
  }

  static Future<InfoMeModel> infoMe(context) async {
    InfoMeModel? infoMeModel;
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();

      var url = Uri.parse('$baseUrlAPI/me');
      String? tokenType = preferences.getString("TOKEN_TYPE");
      String? tokenAccess = preferences.getString("ACCESS_TOKEN");

      var headerMe = {
        'Authorization': '$tokenType $tokenAccess',
        'Accept': 'application/json'
      };

      Response response = await http.get(url, headers: headerMe);
      debugPrint(":::::: statusCode ::::::: " + response.statusCode.toString());
      if (response.statusCode == 200) {
        debugPrint("=-=- Success get Info Me  =-=-");
        final item = json.decode(response.body);
        infoMeModel = InfoMeModel.fromJson(item);
      } else {
        debugPrint("::::: failed to Get Info Me :::::");
        showSnackBarCustom(text: "failed to Get Info Me", context: context);
      }
    } catch (e) {
      debugPrint(e.toString());
    }

    return infoMeModel!;
  }

  static Future<bool> refreshToken(context) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    var url = Uri.parse('$baseUrlAPI/auth/refresh');
    String? tokenType = preferences.getString("TOKEN_TYPE");
    String? tokenAccess = preferences.getString("ACCESS_TOKEN");
    String? refreshToken = preferences.getString("REFRESH_TOKEN");

    var headerRefreshToken = {
      'Authorization': '$tokenType $tokenAccess',
      'Content-Type': 'application/json',
    };
    Map bodyRefreshToken = {"refresh_token": refreshToken};

    Response response = await http.post(url,
        headers: headerRefreshToken, body: jsonEncode(bodyRefreshToken));
    debugPrint(":::::: statusCode ::::::: " + response.statusCode.toString());
    if (response.statusCode == 200) {
      debugPrint("=-=- Success to Refresh Access Token =-=-");
      Map data = jsonDecode(response.body);
      await preferences.setString("TOKEN_TYPE", data['token_type']);
      await preferences.setString("ACCESS_TOKEN", data['access_token']);
      await preferences.setString("REFRESH_TOKEN", data['refresh_token']);
      Navigator.pop(context);
      showSnackBarCustom(
          text: "Success to Refresh Access Token", context: context);
    } else {
      debugPrint("::::: failed to  refresh token :::::");
      Navigator.pop(context);
      showSnackBarCustom(text: "failed to refresh token", context: context);
    }

    return response.statusCode == 200 ? true : false;
  }
}
