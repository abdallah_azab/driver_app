import 'dart:convert';
import 'dart:developer';
import '../Constast/vaiabels.dart';
import '../Helpers/SnackBar/snack_bar.dart';
import '../Model/container_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

// =--=-=-  Get All Routes -=-=--=
Future<ContainersModel> getAllRoutesAPI(context, {page = 1}) async {
  ContainersModel? modelResult;

  try {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String? tokenType = preferences.getString("TOKEN_TYPE");
    String? tokenAccess = preferences.getString("ACCESS_TOKEN");
    debugPrint(" =-=- TOKEN_TYPE =-=-  " + tokenType.toString());
    debugPrint(" =--=-=- TOKEN_ACCESS -=-=-=- " + tokenAccess.toString());
    var header = {'Authorization': '$tokenType $tokenAccess'};

    var url = Uri.parse('$baseUrlAPI/routes?page=$page&&length=10');
    var response = await http.get(url, headers: header);
    debugPrint(":::::: statusCode ::::::: " + response.statusCode.toString());
    if (response.statusCode == 200) {
      final item = json.decode(response.body);
      debugPrint('=-=-=- current_page --=---= ' +
          item['data']['current_page'].toString());
      modelResult = ContainersModel.fromJson(item['data']);
    } else {
      debugPrint("::::: failed to Get All Routes :::::");
      showSnackBarCustom(text: "failed to Get All Routes", context: context);
    }
  } catch (e) {
    log(e.toString());
  }
  return modelResult!;
}

// =-=-=--=   Get Description Rout -=-=-=-
Future<Data> getDescriptionRouteAPI(context, {required id}) async {
  Data? modelResult;

  try {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String? tokenType = preferences.getString("TOKEN_TYPE");
    String? tokenAccess = preferences.getString("ACCESS_TOKEN");
    debugPrint(" =-=- TOKEN_TYPE =-=-  " + tokenType.toString());
    debugPrint(" =--=-=- TOKEN_ACCESS -=-=-=- " + tokenAccess.toString());
    var header = {'Authorization': '$tokenType $tokenAccess'};

    var url = Uri.parse('$baseUrlAPI/routes/$id');
    var response = await http.get(url, headers: header);
    debugPrint(":::::: statusCode ::::::: " + response.statusCode.toString());
    if (response.statusCode == 200) {
      final item = json.decode(response.body);
      modelResult = Data.fromJson(item);
    } else {
      debugPrint("::::: failed to Get Route id $id  :::::");
      showSnackBarCustom(
          text: "failed to Get  Route id $id ", context: context);
    }
  } catch (e) {
    log(e.toString());
  }
  return modelResult!;
}
