// ignore_for_file: avoid_print, unnecessary_null_comparison, unused_element, unused_field

import 'dart:async';

import 'package:driver/Constast/Stayls/colors.dart';
import 'package:driver/Helpers/methods/get_current_location.dart';
import 'package:driver/Model/container_model.dart';
import 'package:driver/UI/screens/HomeScreen/routes/routes_page.dart';
import 'package:driver/UI/widgets/map_card.dart';
import 'package:driver/UI/widgets/thanks_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_tappable_polyline/flutter_map_tappable_polyline.dart';
import 'package:get/get.dart';
import 'package:latlong2/latlong.dart';
import 'package:location/location.dart';

// ignore: must_be_immutable
class MapPage extends StatefulWidget {
  Data route;

  MapPage({Key? key, required this.route}) : super(key: key);

  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  late Data route;

  LocationData? currentLocation;
  double? lat, lng;

  getCurrentLocation() async {
    currentLocation = await HelpLMap.getCurrentLocation();

    setState(() {
      lat = currentLocation!.latitude;
      lng = currentLocation!.longitude;
    });

    debugPrint(" :::: Lat ::::  " +
        lat.toString() +
        " :::::: Lng :::: " +
        lng.toString());
  }

  late LatLng currentPoint;

  int indexCurrentPoint = 0;

  doneTap() {
    if (indexCurrentPoint < route.points.length - 1) {
      setState(() {
        indexCurrentPoint++;
      });
      setData(indexCurrentPoint);
      debugPrint(':::: indexCurrentPoint :::: ' + indexCurrentPoint.toString());
    }else{
      showThanksDialog();
    }
  }
  bool showThanks = false ;
  showThanksDialog(){
    setState(() {
      showThanks =true ;
    });
  }


  @override
  void initState() {
    super.initState();
    setDataItail();
    // -=-=-=-  Get Current Location -=-=-=-=-=-
    getCurrentLocation();
    _listenLocation();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: lat == null || lng == null
            ? const Center(
                child: CupertinoActivityIndicator(
                  radius: 15,
                  animating: true,
                ),
              )
            : Stack(
                children: [
                  FlutterMap(
                    options: MapOptions(
                      plugins: [TappablePolylineMapPlugin()],
                      allowPanning: true,
                      center: LatLng(lat!, lng!),
                      zoom: 14.0,
                    ),
                    layers: [
                      // PolylineLayerOptions(
                      //     polylineCulling: true,
                      //   polylines: [
                      //     Polyline(
                      //       color: ColorsApp.defaultColor,
                      //       borderStrokeWidth: 4,
                      //       borderColor: ColorsApp.defaultColor,
                      //       strokeWidth: 4,
                      //       points: [
                      //         LatLng(lat!, lng!),
                      //         currentPoint
                      //       ]
                      //     )
                      //   ]
                      // ),

                      TappablePolylineLayerOptions(
                          // Will only render visible polylines, increasing performance
                          polylineCulling: true,
                          polylines: [
                            TaggedPolyline(
                                tag: "My Polyline",
                                points: [LatLng(lat!, lng!), currentPoint],
                                color: Colors.amber,
                                strokeWidth: 9.0
                                // An optional tag to distinguish polylines in callback
                                // ...all other Polyline options
                                ),
                          ],
                          onTap: (polylines, tapPosition) => print('Tapped: ' +
                              polylines
                                  .map((polyline) => polyline.tag)
                                  .join(',') +
                              ' at ' +
                              tapPosition.globalPosition.toString()),
                          onMiss: (tapPosition) {
                            print('No polyline was tapped at position ' +
                                tapPosition.globalPosition.toString());
                          }),

                      TileLayerOptions(
                        urlTemplate:
                            "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                        subdomains: ['a', 'b', 'c'],
                      ),
                      MarkerLayerOptions(markers: [
                        // =-=-=-=-=-  Marker current Location =-=-=-=--
                        Marker(
                          point: LatLng(lat!, lng!),
                          builder: (ctx) => InkWell(
                            onTap: () {
                              debugPrint(":::: Marker Tapped :::::");
                            },
                            child: Icon(
                              Icons.location_on,
                              size: 30,
                              color: ColorsApp.defaultColor,
                            ),
                          ),
                        ),

                        Marker(
                          point: currentPoint,
                          builder: (ctx) => InkWell(
                              onTap: () {
                                debugPrint(":::: Marker Tapped :::::");
                              },
                              child: const Icon(
                                Icons.location_on,
                                size: 30,
                                color: Colors.red,
                              )


                              ),
                        ),
                      ])
                    ],
                  ),
                  Positioned(
                    bottom: -5.0,
                    left: 5,
                    right: 5,
                    child:

                    showThanks == true ? Thanks_Card(context: context,onTap: (){
                      Get.offAll(const  RoutePage());
                    })
                    :Map_Card(
                        context: context,
                        distance: '150 Km',
                        doneTap: () {
                          print("++++ Done Tapped +++");
                          doneTap();
                        },
                        skipTap: () {},
                        time: widget.route.startTime),
                  )
                ],
              ),
      ),
    );
  }

  setDataItail() {
    setState(() {
      route = widget.route;
      currentPoint =
          LatLng(route.points[0].latitude, route.points[0].longitude);
    });
  }

  setData(index) {
    setState(() {
      if (route.points[index].latitude != null ||
          route.points[index].longitude != null ||
          route != null) {
        currentPoint =
            LatLng(route.points[index].latitude, route.points[index].longitude);
      }
    });
    debugPrint(':::: Current Point ::::' +
        currentPoint.latitude.toString() +
        ':::::' +
        currentPoint.longitude.toString());
  }

  // -=-=-=- -=-=-=-= -=-=-=-  -=-=-=-=- -=-=--

  final Location location = Location();
  StreamSubscription<LocationData>? _locationSubscription;
  String? _error;
  LocationData? _location;

  Future<void> _listenLocation() async {
    _locationSubscription =
        location.onLocationChanged.handleError((dynamic err) {
      if (err is PlatformException) {
        setState(() {
          _error = err.code;
        });
      }
      _locationSubscription?.cancel();
      setState(() {
        _locationSubscription = null;
      });
    }).listen((LocationData currentLocation) {
      setState(() {
        _error = null;
        _location = currentLocation;
        lat = _location!.latitude;
        lng = _location!.longitude;
      });
      debugPrint(":::: Listen To Current Location :::: " +
          lat.toString() +
          " +_+_+_+- " +
          lng.toString());
    });
    setState(() {});
  }

  Future<void> _stopListen() async {
    _locationSubscription?.cancel();
    setState(() {
      _locationSubscription = null;
    });
  }

  @override
  void dispose() {
    _locationSubscription?.cancel();
    _locationSubscription = null;
    super.dispose();
  }
}
