import 'package:delayed_display/delayed_display.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';

import '../HomeScreen/home_page.dart';
import '../login/login_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final Duration initialDelay = const Duration(seconds: 1);
  bool show = false;

  bool? isLogin;

  Future<bool> getIsLogin() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      isLogin = preferences.getBool('ISLOGIN');
    });
    debugPrint(":::: isLogin ::::" + isLogin.toString());
    return isLogin ?? false;
  }

  @override
  void initState() {
    super.initState();
    getIsLogin();
    Future.delayed(
      const Duration(seconds: 3),
    ).then((value) {
      Get.off(isLogin == true ? const HomeScreen() : const LoginScreen());
      // Get.off(const LoginScreen());
    });
  }

  bool successLogin = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff213152),
      body: SafeArea(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 15,
                ),
                DelayedDisplay(
                  slidingBeginOffset: const Offset(0.0, 2),
                  delay: const Duration(seconds: 1),
                  fadeIn: true,
                  fadingDuration: const Duration(seconds: 2),
                  child: Container(
                    height: 200,
                    width: 200,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                    ),
                    child: Center(
                      child: Image.asset(
                        'assets/images/track.png',
                        width: 50.w,
                        height: 21.h,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                /*

                DelayedDisplay(
                  slidingBeginOffset: const Offset(0.0, 1),
                  delay: const Duration(seconds: 2),
                  fadeIn: true,
                  fadingDuration: const Duration(seconds: 2),
                  child: InkWell(
                    onTap: () {
                      ServerApiAuth.loginApi(
                          context: context,
                          userName: 'mohamed.hessen@ix-sol.com',
                          password: '123456789');
                    },
                    child: Container(
                      // width: 100,
                      // height: 100,
                      color: Colors.lightGreen,
                      alignment: Alignment.center,

                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: successLogin == false
                                ? const Text(
                                    "Sing in to your account",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                  )
                                : const CircularProgressIndicator(),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),

                */
              ],
            ),
          ],
        ),
      ),
    );
  }
}
