import '../HomeScreen/home_page.dart';
import '../../widgets/text_field.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../Constast/Stayls/colors.dart';
import '../../../../Helpers/SnackBar/snack_bar.dart';
import '../../../../Helpers/methods/controller_keyboard.dart';
import '../../../../server_APIs/api_auth.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../widgets/buttons/button_text.dart';

class OtpScreen extends StatefulWidget {
  const OtpScreen({Key? key}) : super(key: key);

  @override
  State<OtpScreen> createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  var formKey = GlobalKey<FormState>();

  var emailController = TextEditingController();

  var optController = TextEditingController();

  var passwordController = TextEditingController();

  bool showLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: ColorsApp.darkBlue,
      ),
      backgroundColor: ColorsApp.darkBlue,
      body: SingleChildScrollView(
        physics: const NeverScrollableScrollPhysics(),
        child: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                top: 85,
                left: 15,
                right: 15,
              ),
              child: SizedBox(
                height: MediaQuery.of(context).size.height / 2,
                width: MediaQuery.of(context).size.height / 1.80,
                child: Card(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      top: 50,
                      bottom: 20,
                      left: 20,
                      right: 20,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const SizedBox(height: 80),
                        //  TEXT
                        Text(
                          'OPT LOGIN',
                          style: TextStyle(
                            fontSize: 18.sp,
                            color: ColorsApp.defaultColor,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(height: 60),
                        // USER FormField
                        TextFieldCustom(
                          hintText: 'OTP Number',
                          controller: emailController,
                          icon: Icons.mail,
                          textValidator: "",
                        ),
                        const SizedBox(height: 65),

                        // LOGIN Button

                        showLoading == false
                            ? ButtonWithTextApp(
                                title: "SIGN IN",
                                onTap: () {
                                  if (emailController.text.isEmpty ||
                                      passwordController.text.isEmpty) {
                                    showSnackBarCustom(
                                        context: context,
                                        text: emailController.text.isEmpty
                                            ? "the OTP can't be null"
                                            : "the password can't be null ");
                                  } else {
                                    setState(
                                      () {
                                        showLoading = true;
                                      },
                                    );
                                    ServerApiAuth.loginApi(
                                            context: context,
                                            userName: emailController.text,
                                            password: passwordController.text)
                                        .then(
                                      (value) {
                                        if (value == true) {
                                          setState(
                                            () {
                                              showLoading = false;
                                            },
                                          );
                                          Get.off(const HomeScreen());

                                          saveLogin();
                                        } else {
                                          setState(
                                            () {
                                              showLoading = false;
                                            },
                                          );
                                        }
                                      },
                                    );
                                  }
                                  hideKeyboard(context);
                                },
                              )
                            : CircularProgressIndicator(
                                color: ColorsApp.defaultColor,
                              ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            // Icon
            Center(
              child: Image.asset(
                'assets/images/track.png',
                width: 50.w,
                height: 21.h,
              ),
            ),
          ],
        ),
      ),
    );
  }

  saveLogin() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool('ISLOGIN', true);
    debugPrint(
        "::::: isLogin ::::" + preferences.getBool('ISLOGIN').toString());
  }
}
