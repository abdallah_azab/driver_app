import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../Constast/Stayls/colors.dart';
import '../../../Helpers/SnackBar/snack_bar.dart';
import '../../../Helpers/methods/controller_keyboard.dart';
import '../HomeScreen/home_page.dart';
import '../../widgets/buttons/button_text.dart';
import '../../widgets/text_field.dart';
import '../../../server_APIs/api_auth.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var formKey = GlobalKey<FormState>();

  var emailController = TextEditingController();

  var passwordController = TextEditingController();

  bool showLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: ColorsApp.darkBlue,
      ),
      backgroundColor: ColorsApp.darkBlue,
      body: SingleChildScrollView(
        physics: const NeverScrollableScrollPhysics(),
        child: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                top: 85,
                left: 15,
                right: 15,
              ),
              child: SizedBox(
                height: MediaQuery.of(context).size.height / 1.70,
                width: 370,
                child: Card(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      top: 50,
                      bottom: 20,
                      left: 20,
                      right: 20,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const SizedBox(height: 45),
                        //  TEXT
                        Text(
                          'sign in to your account',
                          style: TextStyle(
                            fontSize: 17.sp,
                            color: ColorsApp.defaultColor,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(height: 45),
                        // USER FormField

                        TextFieldCustom(
                          hintText: 'User Name',
                          controller: emailController,
                          icon: Icons.person,
                          textValidator: "",
                        ),
                        const SizedBox(height: 20),
                        // PASSWORD FormField

                        TextFieldCustom(
                          hintText: 'Password',
                          icon: Icons.lock_outline,
                          controller: passwordController,
                          textValidator: "",
                          isPassWord: true,
                        ),

                        // LOGIN Button

                        const Expanded(child: SizedBox()),

                        showLoading == false
                            ? ButtonWithTextApp(
                                title: "SIGN IN",
                                onTap: () {
                                  if (emailController.text.isEmpty ||
                                      passwordController.text.isEmpty) {
                                    showSnackBarCustom(
                                        context: context,
                                        text: emailController.text.isEmpty
                                            ? "the email can't be null"
                                            : "the password can't be null ");
                                  } else {
                                    setState(() {
                                      showLoading = true;
                                    });
                                    ServerApiAuth.loginApi(
                                            context: context,
                                            userName: emailController.text,
                                            password: passwordController.text)
                                        .then((value) {
                                      if (value == true) {
                                        setState(() {
                                          showLoading = false;
                                        });
                                        // Navigator.pushReplacement(
                                        //   context,
                                        //   MaterialPageRoute(
                                        //     builder: (context) =>
                                        //         const HomePage(),
                                        //   ),
                                        // );
                                        Get.off(
                                          const HomeScreen(),

                                          // const OtpScreen(),
                                        );

                                        saveLogin();
                                      } else {
                                        setState(() {
                                          showLoading = false;
                                        });
                                      }
                                    });
                                  }
                                  hideKeyboard(context);
                                },
                              )
                            : CircularProgressIndicator(
                                color: ColorsApp.defaultColor,
                              )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            // Icon
            Center(
              child: Image.asset(
                'assets/images/track.png',
                width: 50.w,
                height: 21.h,
              ),
            ),
          ],
        ),
      ),
    );
  }

  saveLogin() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool('ISLOGIN', true);
    debugPrint(
        "::::: isLogin ::::" + preferences.getBool('ISLOGIN').toString());
  }
}
