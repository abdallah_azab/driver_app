import '../../../../Constast/image/images_app.dart';
import '../../../widgets/app_bar.dart';
import '../../../widgets/nav_bar.dart';
import '../../../widgets/percent_indicator_circle.dart';
import 'package:flutter/material.dart';

class InfoGraphPage extends StatefulWidget {
  const InfoGraphPage({Key? key}) : super(key: key);

  @override
  _InfoGraphPageState createState() => _InfoGraphPageState();
}

class _InfoGraphPageState extends State<InfoGraphPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Column(
              children: [
                Align(
                  alignment: Alignment.topCenter,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 35),
                    child: AppBarCustom(
                        title: "Info Graph",
                        showIcon: true,
                        icon: Images.infoGraph),
                  ),
                ),
                const Spacer(),
                Align(
                  alignment: Alignment.center,
                  child: PercentCircle(
                    percent: 0.75,
                  ),
                ),
                const Spacer(),
              ],
            ),
            Positioned(bottom: 0.0, child: Nav_Bar(context: context))
          ],
        ),
      ),
    );
  }
}
