import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

import '../../../Constast/image/images_app.dart';
import '../../../Helpers/methods/show_pop_scope.dart';
import '../../../provider/provider_info_me.dart';
import '../../widgets/app_bar.dart';
import '../../widgets/buttons/icon_with_text.dart';
import '../../widgets/drawer.dart';
import 'infoGraph/info_graph_page.dart';
import 'reports/reports_page.dart';
import 'routes/routes_page.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _keyScaffold = GlobalKey();

  getInfo() {
    Provider.of<InfoMeProvider>(context, listen: false).getInfoMe(context);
  }

  @override
  void initState() {
    super.initState();
    getInfo();
  }

  @override
  Widget build(BuildContext context) {
    return PopScopeApp(
      child: Scaffold(
        key: _keyScaffold,
        drawer: drawerApp(context: context),
        body: SafeArea(
          child: Column(
            children: [
              // =---==- App Bar home Page =-=-=
              SizedBox(
                height: 9.h,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    // =-=-=- the Image -=-=- Drawer =-=-=
                    Padding(
                        padding: const EdgeInsets.only(top: 3),
                        child: circleImage(openDrawer: () {
                          setState(() {
                            if (_keyScaffold.currentState!.isDrawerOpen) {
                              _keyScaffold.currentState!.openEndDrawer();
                            } else {
                              _keyScaffold.currentState!.openDrawer();
                            }
                          });
                        })),
                    Expanded(
                        child: AppBarCustom(
                      title: "HOME",
                    ))
                  ],
                ),
              ),

              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      IconWithText(
                          title: "Routes",
                          icon: Images.greenRoute,
                          onTap: () => Get.to(const RoutePage())),
                      IconWithText(
                        title: "Reports",
                        icon: Images.reports,
                        onTap: () => Get.to(const ReportsPage()),
                      ),
                      IconWithText(
                        title: "IngoGraph",
                        icon: Images.infoGraphGreen,
                        onTap: () => Get.to(const InfoGraphPage()),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget circleImage({openDrawer}) {
    return InkWell(
      onTap: openDrawer ?? () {},
      child: Container(
        height: 9.h,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
        ),
        child: Center(
            child: Image.asset(
          Images.person,
          fit: BoxFit.fill,
        )),
      ),
    );
  }
}
