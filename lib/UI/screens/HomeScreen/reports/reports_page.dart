import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../Constast/Stayls/colors.dart';
import '../../../../Constast/image/images_app.dart';
import '../../../../provider/provider_routes.dart';
import '../../../widgets/app_bar.dart';
import '../../../widgets/nav_bar.dart';
import '../../../widgets/route_widget.dart';

class ReportsPage extends StatefulWidget {
  const ReportsPage({Key? key}) : super(key: key);

  @override
  _ReportsPageState createState() => _ReportsPageState();
}

class _ReportsPageState extends State<ReportsPage> {
  @override
  Widget build(BuildContext context) {
    final providerGetRoutes = Provider.of<RoutesProvider>(context);
    return RefreshIndicator(
      onRefresh: () => providerGetRoutes.getAllRoutes(context),
      child: Scaffold(
        body: SafeArea(
          child: Stack(
            children: [
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 35),
                    child: AppBarCustom(
                        title: "Reports", showIcon: true, icon: Images.reports),
                  ),
                  providerGetRoutes.loadingAllRoutes
                      ? Expanded(
                          child: Center(
                            child: CircularProgressIndicator(
                              color: ColorsApp.defaultColor,
                            ),
                          ),
                        )
                      : Expanded(
                          child: SizedBox(
                            child: ListView.builder(
                              shrinkWrap: true,
                              itemCount:
                                  providerGetRoutes.modelAllRoute!.data.length,
                              itemBuilder: (context, index) => Routes_Card(
                                  isReports: true,
                                  state: index % 2 == 1 ? 1 : 0,
                                  context: context,
                                  function: () {
                                    // Get.to(DetailsRout(
                                    //   dataRoute: providerGetRoutes
                                    //       .modelAllRoute!.data[index],
                                    // ));
                                  },
                                  name: providerGetRoutes
                                      .modelAllRoute!.data[index].name,
                                  points: providerGetRoutes
                                      .modelAllRoute!.data[index].points.length,
                                  time: providerGetRoutes
                                      .modelAllRoute!.data[index].startTime),
                            ),
                          ),
                        )
                ],
              ),
              Positioned(bottom: 0.0, child: Nav_Bar(context: context))
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    // =-=-=---=-  To Get Data -=-=-=-
    final providerGetRoutes =
        Provider.of<RoutesProvider>(context, listen: false);
    providerGetRoutes.getAllRoutes(context);
  }
}
