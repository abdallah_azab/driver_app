import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

import '../../../../Constast/Stayls/colors.dart';
import '../../../../Constast/image/images_app.dart';
import '../../../../Model/container_model.dart';
import '../../../widgets/details_widget.dart';
import '../../../widgets/nav_bar.dart';
import '../../../widgets/point_card.dart';
import '../../../widgets/text.dart';
import '../../Map/map_page.dart';

// ignore: must_be_immutable
class DetailsRout extends StatelessWidget {
  Data dataRoute;

  DetailsRout({Key? key, required this.dataRoute}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Column(
              children: [
                // -=-=-=- Header =-=-=-
                _headerInfo(),
                // =-=-=-=-  Text Points =-=--
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 18, vertical: 15),
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: MyText(
                          title: 'Points',
                          weight: FontWeight.w600,
                          size: 15.sp)),
                ),
                // =-=-=- List Of Points -=-=-=-
                Expanded(
                    child: SizedBox(
                  child: ListView.builder(
                    itemCount: dataRoute.points.length,
                    itemBuilder: (context, index) => Points_Card(
                        function: () {},
                        context: context,
                        lng: dataRoute.points[index].longitude.toString(),
                        lat: dataRoute.points[index].latitude.toString(),
                        pointsId: dataRoute.points[index].routeId,
                        pointsName: dataRoute.points[index].name,
                        pointsType: dataRoute.points[index].type),
                  ),
                ))
              ],
            ),
            Positioned(bottom: 0.0, child: Nav_Bar(context: context))
          ],
        ),
      ),
    );
  }

  Widget _headerInfo() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 1.5, horizontal: 3),
      child: Container(
        height: 20.h,
        decoration: BoxDecoration(
          color: ColorsApp.offlight.withOpacity(0.6),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            rowInfoWithIconStart(
                start: (){
                  Get.to(MapPage(route: dataRoute,));
                }
            ),
            rowInfo(
                title1: 'Company Id',
                supTitle1: dataRoute.companyId.toString(),
                title2: 'Description',
                supTitle2: dataRoute.description),
            rowInfo(
                title1: 'End Time',
                supTitle1: dataRoute.endTime,
                title2: 'Start Time',
                supTitle2: dataRoute.startTime),
          ],
        ),
      ),
    );
  }

  Widget rowInfoWithIconStart({required start}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 6),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          textRow(
            title1: 'Name',
            title2: dataRoute.name,
          ),
          InkWell(
            onTap: start,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Card(
                color: ColorsApp.lightOrange,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                elevation: 1.5,
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        Images.start,
                        height: 2.5.h,
                      ),
                      const SizedBox(
                        width: 4,
                      ),
                      Text(
                        ' Start ',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14.sp,
                            fontWeight: FontWeight.w600),
                      )
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
