import 'package:driver/Model/container_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../../Constast/Stayls/colors.dart';
import '../../../../Constast/image/images_app.dart';
import '../../../../provider/provider_routes.dart';
import '../../../widgets/app_bar.dart';
import '../../../widgets/nav_bar.dart';
import '../../../widgets/route_widget.dart';
import 'details_route.dart';

class RoutePage extends StatefulWidget {
  const RoutePage({Key? key}) : super(key: key);

  @override
  State<RoutePage> createState() => _RoutePageState();
}

class _RoutePageState extends State<RoutePage> {
  @override
  Widget build(BuildContext context) {
    final providerGetRoutes = Provider.of<RoutesProvider>(context);
    List<Data>? date = providerGetRoutes.data;
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 35),
                  child: AppBarCustom(
                      title: "Routes", showIcon: true, icon: Images.blueRoute),
                ),
                providerGetRoutes.loadingAllRoutes
                    ? Expanded(
                        child: Center(
                          child: CircularProgressIndicator(
                            color: ColorsApp.defaultColor,
                          ),
                        ),
                      )
                    : Expanded(
                        child: SizedBox(
                          child: SmartRefresher(
                            scrollController: scrollController,
                            controller: refreshController,
                            scrollDirection: Axis.vertical,
                            enablePullDown: true,
                            enablePullUp: true,
                            onLoading: () async {
                              print(" -=-=- -=-=-   loading -=-=-=-=- =-=-");
                              await getRoutes();
                              refreshController.loadComplete();
                            },
                            onRefresh: () async {
                              print(" -=-=- -=-=-   refresh-=-=-=-=- =-=-");
                              await getRoutes();
                              refreshController.refreshCompleted();
                            },
                            child: ListView.builder(
                              controller: scrollController,
                              shrinkWrap: true,
                              itemCount: date!.length,
                              itemBuilder: (context, index) => Routes_Card(
                                  context: context,
                                  function: () {
                                    Get.to(DetailsRout(
                                      dataRoute: date[index],
                                    ));
                                  },
                                  name: date[index].name,
                                  points: date[index].points.length,
                                  time: date[index].startTime),
                            ),
                          ),
                        ),
                      ),
                const SizedBox(
                  height: 10,
                )
              ],
            ),
            Positioned(bottom: 0.0, child: Nav_Bar(context: context))
          ],
        ),
      ),
    );
  }

  final RefreshController refreshController = RefreshController(
    initialRefresh: false,
  );
  ScrollController scrollController = ScrollController();
  late RoutesProvider providerGetRoutes;

  int page = 1;

  Future getRoutes() async {
    // if(page ==1 ){
    //   providerGetRoutes.getAllRoutes(context,page: page);
    // }else{
    //   providerGetRoutes.getAllRoutes(context,page: page);
    // }
    // page ++ ;
    providerGetRoutes.getAllRoutes(context);

    print('=-=--= page -=-=-=- ' + providerGetRoutes.page.toString());
  }

  @override
  void initState() {
    super.initState();
    // =-=-=---=-  To Get Data -=-=-=-
    // final providerGetRoutes = Provider.of<RoutesProvider>(context, listen: false);
    providerGetRoutes = Provider.of<RoutesProvider>(context, listen: false);
    getRoutes();
    // providerGetRoutes.getAllRoutes(context,page:page);
  }
}
