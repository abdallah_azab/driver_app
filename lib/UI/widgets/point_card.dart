// ignore_for_file: unnecessary_string_interpolations

import '../../Constast/Stayls/colors.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

// ignore: non_constant_identifier_names
Widget Points_Card({
  required String pointsName,
  required int pointsId,
  required String pointsType,
  required String lng,
  required String lat,
  required Function function,
  required BuildContext context,
}) =>
    Padding(
      padding: const EdgeInsets.only(
        bottom: 10,
        right: 7,
        left: 7,
      ),
      child: Container(
        width: MediaQuery.of(context).size.width,
        // height: 140,
        height: 17 .h,
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        child: Card(
          elevation: 5,
          shadowColor: Colors.black,
          color: const Color(0xffFFFFFF),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    _rowInfo(title1: 'Points Name : ', title2: pointsName),
                    _rowInfo(
                        title1: 'Points ID : ', title2: pointsId.toString()),
                    _rowInfo(title1: 'Points Type : ', title2: pointsType),
                    _rowInfo(title1: 'Late : ', title2: '$lat'),
                    _rowInfo(title1: 'Long : ', title2: '$lng'),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );

Widget _rowInfo({title1, title2}) {
  return Row(
    children: [
      Text(
        '$title1 ',
        style: TextStyle(
          color: ColorsApp.defaultColor,
          fontSize: 14.sp,
        ),
      ),
      Text(
        title2.toString(),
        style: TextStyle(
          color: ColorsApp.lightOrange,
          fontSize: 14.sp,
          fontWeight: FontWeight.w500,
        ),
      ),
    ],
  );
}
