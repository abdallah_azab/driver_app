import '../../Constast/Stayls/colors.dart';
import 'text.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:sizer/sizer.dart';

// ignore: must_be_immutable
class PercentCircle extends StatelessWidget {
  double percent;

  PercentCircle({Key? key, this.percent = 0.0}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        CircularPercentIndicator(
          backgroundColor: ColorsApp.defaultColor,
          restartAnimation: true,
          radius: 24.h,
          lineWidth: 9.w,
          animation: true,
          percent: percent,
          center: MyText(
              title: "${(percent * 100).toInt()}%",
              color: ColorsApp.deepOrange,
              weight: FontWeight.bold,
              size: 17.sp),
          circularStrokeCap: CircularStrokeCap.butt,
          progressColor: ColorsApp.green,
        ),
        SizedBox(
          height: 12.1.h,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              _infoRow('Completed', ColorsApp.defaultColor),
              _infoRow('In Progress', ColorsApp.green),
            ],
          ),
        )
      ],
    );
  }

  Widget _infoRow(title, color) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 8),
          height: 4.h,
          width: 4.w,
          decoration: BoxDecoration(color: color, shape: BoxShape.circle),
        ),
        MyText(title: title, color: Colors.black, weight: FontWeight.w500)
      ],
    );
  }
}
