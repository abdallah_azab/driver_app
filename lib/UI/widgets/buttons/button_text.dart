// ignore_for_file: must_be_immutable, prefer_typing_uninitialized_variables

import 'package:driver/Constast/Stayls/colors.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class ButtonWithTextApp extends StatelessWidget {
  String title;
  var onTap;
  var hight;
  var width;

  ButtonWithTextApp(
      {Key? key, required this.title, this.onTap, this.hight, this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap ?? () {},
      child: Container(
        height: hight ?? 6.h,
        width: width ?? 35.w,
        decoration: BoxDecoration(
          color: ColorsApp.green,
          borderRadius: BorderRadius.circular(15),
        ),
        child: Center(
          child: Text(
            title,
            style: TextStyle(
              color: Colors.white,
              fontSize: 13.sp,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
