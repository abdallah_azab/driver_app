// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:driver/Constast/Stayls/colors.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

// ignore: must_be_immutable
class IconWithText extends StatelessWidget {
  String icon;

  String title;

  var onTap;

  IconWithText({Key? key, required this.title, required this.icon, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap ?? () {},
      child: SizedBox(
        width: 30.w,
        height: 9.5.h,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Image.asset(icon),
            Text(
              title,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15.sp,
                  color: ColorsApp.green),
            )
          ],
        ),
      ),
    );
  }
}
