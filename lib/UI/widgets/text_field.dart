import '../../Constast/Stayls/colors.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

// ignore: must_be_immutable
class TextFieldCustom extends StatefulWidget {
  TextEditingController controller;
  IconData icon;
  String? textValidator;
  bool isPassWord;
  String ?hintText;

  TextFieldCustom(
      {Key? key,
      required this.controller,
      this.hintText ='' ,
      required this.icon,
      this.isPassWord = false,
      this.textValidator})
      : super(key: key);

  @override
  State<TextFieldCustom> createState() => _TextFieldCustomState();
}

class _TextFieldCustomState extends State<TextFieldCustom> {
  bool emptyText = true;
  bool showPass = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 7.h,
      decoration: BoxDecoration(
        color: emptyText
            ? ColorsApp.tail.withOpacity(0.25)
            : ColorsApp.darkBlue.withOpacity(0.2),
        borderRadius: BorderRadius.circular(10),
        // border: Border()
      ),
      child: TextField(
        autofocus: false,
        obscureText: widget.isPassWord == true ? showPass : false,
        enableSuggestions: widget.isPassWord == true ? false : true,
        autocorrect: false,
        onChanged: (value) {
          setState(() {
            emptyText = value.isEmpty ? true : false;
          });
        },
        style: TextStyle(
          color: ColorsApp.defaultColor,
        ),
        controller: widget.controller,
        keyboardType: widget.isPassWord == true
            ? TextInputType.visiblePassword
            : TextInputType.emailAddress,
        decoration: InputDecoration(
            fillColor: ColorsApp.green,
            prefixIcon: Icon(
              widget.icon,
              color: emptyText ? ColorsApp.offlight : ColorsApp.defaultColor,
            ),
            // hintText: widget.isPassWord ? "Password" : 'User Name' ,
            hintText:  widget.hintText,
            // widget.isPassWord ? "Password" : 'User Name' ,

            suffixIcon: widget.isPassWord == true
                ? InkWell(
                    onTap: () {
                      setState(() {
                        showPass = !showPass;
                      });
                    },
                    child: Icon(
                      showPass == false
                          ? Icons.remove_red_eye_outlined
                          : Icons.visibility_off_outlined,
                      color: Colors.grey,
                    ),
                  )
                : const SizedBox(),
            hintStyle: TextStyle(
                color: widget.controller.text.isEmpty
                    ? ColorsApp.grey
                    : ColorsApp.defaultColor),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.circular(10)),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide.none,
            ),
            hoverColor: ColorsApp.green),
      ),
    );
  }
}
