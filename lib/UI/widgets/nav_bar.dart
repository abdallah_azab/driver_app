// ignore_for_file: non_constant_identifier_names
import 'package:driver/Constast/Stayls/colors.dart';
import 'package:driver/UI/screens/HomeScreen/home_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

Widget Nav_Bar({
  required BuildContext context,
}) =>
    Container(
      height: 7.h,
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(
        border: Border(
          top: BorderSide(width: 1.5, color: Colors.grey),
        ),
        color: Colors.white,
      ),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            // arrow_back
            Nav_Bar_Icon(
              Tap: () => Get.back(),
              icon: Icons.arrow_back,
            ),
            // Home
            Nav_Bar_Icon(
              Tap: () => Get.offAll(const HomeScreen()),
              icon: Icons.home,
            ),
          ],
        ),
      ),
    );

Widget Nav_Bar_Icon({
  required var Tap,
  required IconData icon,
}) =>
    InkWell(
      onTap: Tap ?? () {},
      child: Icon(
        icon,
        color: ColorsApp.defaultColor,
        size: 32,
      ),
    );
