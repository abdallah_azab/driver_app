import 'package:flutter/cupertino.dart';

import '../../Model/info_me.dart';
import '../../provider/provider_info_me.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../Constast/Stayls/colors.dart';
import '../../Constast/image/images_app.dart';
import '../../server_APIs/api_auth.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

drawerApp({required BuildContext context}) {
  var info = Provider.of<InfoMeProvider>(context);
  // InfoMeModel? infoModel = Provider.of<InfoMeProvider>(context).infoMeModel;
  InfoMeModel? infoModel = info.infoMeModel;
  return Padding(
    padding: const EdgeInsets.all(0.5),
    child: ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: Drawer(
        elevation: 20,
        child: Container(
          color: ColorsApp.defaultColor,
          child: Column(
            children: [
              // -=-=-- the image  -=-=-
              Expanded(
                  flex: 2,
                  child: imageProfile(
                      name: info.loadingInfo == true
                          ? ''
                          : infoModel!.data.name)),
              Expanded(
                  flex: 5,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5),
                    child: info.loadingInfo == true
                        ? const Center(
                            // child: CircularProgressIndicator,
                            child: CupertinoActivityIndicator(),
                          )
                        : Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              _rowInfo(
                                  icon: Images.personId,
                                  title: infoModel!.data.personId.toString()),
                              _rowInfo(
                                  icon: Images.phoneIcon, title: "not at api"),
                              _rowInfo(
                                  icon: Images.carType,
                                  title: infoModel.data.carType.toString()),
                              _rowInfo(
                                  icon: Images.carId,
                                  title: infoModel.data.carId.toString()),
                              InkWell(
                                onTap: () {
                                  ServerApiAuth.refreshToken(context);
                                },
                                child: _rowInfo(
                                    icon: Images.refresh,
                                    title: "update Token"),
                              ),
                              InkWell(
                                  onTap: () {
                                    ServerApiAuth.logOut(context);
                                    saveLogOut();
                                  },
                                  child: _rowInfo(
                                      icon: Images.logout, title: "Log Out")),
                              SizedBox(
                                height: 0.5.h,
                              )
                            ],
                          ),
                  ))
            ],
          ),
        ),
      ),
    ),
  );
}

saveLogOut() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  preferences.setBool('ISLOGIN', false);
  debugPrint("::::: isLogin -- logout::::" +
      preferences.getBool('ISLOGIN').toString());
}

Widget imageProfile({image, name}) {
  return Padding(
    padding: EdgeInsets.only(top: 5.h),
    child: Column(
      children: [
        Container(
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
          ),
          child: Image.asset(image ?? Images.personDrawer),
        ),
        _text(title: name ?? "", size: 17.sp)
      ],
    ),
  );
}

Widget _text({title, size}) {
  return Text(
    title ?? "",
    style: TextStyle(
        fontSize: size ?? 13.5.sp,
        fontWeight: FontWeight.w500,
        color: Colors.white70),
  );
}

Widget _rowInfo({required String icon, required String title}) {
  return Row(
    children: [
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25),
        child: Image.asset(
          icon,
          height: 3.5.h,
        ),
      ),
      _text(title: title)
    ],
  );
}
