import '../../Constast/Stayls/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:sizer/sizer.dart';

// ignore: non_constant_identifier_names
Widget MyText({title = '', color, size, weight}) {
  return Padding(
    padding: const EdgeInsets.all(0.5),
    child: Text(
      title,
      style: TextStyle(
          color: color ?? ColorsApp.defaultColor,
          fontSize: size ?? 14.sp,
          fontWeight: weight ?? FontWeight.w400),
    ),
  );
}
