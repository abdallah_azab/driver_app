import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../Constast/Stayls/colors.dart';
import '../../Constast/image/images_app.dart';

// ignore: non_constant_identifier_names
Widget Routes_Card({
  required BuildContext context,
  required String name,
  required int points,
  required String time,
  var function,
  int state = -1,
  bool isReports = false,
}) =>
    Padding(
      padding: const EdgeInsets.all(4),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 12.5.h,
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        child: InkWell(
          onTap: function ?? () {},
          child: Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
              borderRadius: const BorderRadius.all(
                Radius.circular(15),
              ),
              side: BorderSide(
                width: 1.5,
                color: isReports == true
                    ? state == 1
                        ? Colors.green
                        : ColorsApp.lightOrange
                    : Colors.grey.withOpacity(0.3),
              ),
            ),
            shadowColor: Colors.black,
            color: const Color(0xffFFFFFF),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      _rowInfo(title1: 'Name', title2: name),
                      _rowInfo(title1: 'Points no', title2: points),
                      _rowInfo(title1: 'Start Time', title2: time),
                    ],
                  ),
                ),
                isReports != true
                    ? IconButton(
                        icon: Image.asset(
                          Images.info,
                        ),
                        onPressed: () {},
                      )
                    : Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: Text(
                          state == 1 ? "Confirmed" : "Skipped",
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 13.5.sp,
                            color: state == 1
                                ? Colors.green
                                : ColorsApp.deepOrange,
                          ),
                        ),
                      ),
              ],
            ),
          ),
        ),
      ),
    );

Widget _rowInfo({title1, title2}) {
  return Row(
    children: [
      Text(
        '$title1 : ',
        style: TextStyle(
          color: ColorsApp.defaultColor,
          fontSize: 14.sp,
        ),
      ),
      Text(
        title2.toString(),
        style: TextStyle(
          color: Colors.orange,
          fontSize: 14.01.sp,
        ),
      ),
    ],
  );
}
