import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../Constast/Stayls/colors.dart';
import '../screens/HomeScreen/home_page.dart';

// ignore: non_constant_identifier_names
Widget Thanks_Card({
  required BuildContext context,
  required onTap,
  var hight,
  var width,
}) =>
    Padding(
      padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
      child: Container(
        // width: MediaQuery.of(context).size.width,
        height: 15.h,

        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        child: Card(
          elevation: 5,
          shadowColor: Colors.black,
          color: const Color(0xffFFFFFF),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Text(
                  'Thank you , your tasks have been done successfully',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    overflow: TextOverflow.clip,
                    fontSize: 14.sp,
                    color: ColorsApp.defaultColor,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              InkWell(
                onTap: onTap,
                child: Container(
                  // height: hight ?? 8.h,
                  width: width ?? 35.w,
                  decoration: BoxDecoration(
                    color: ColorsApp.lightOrange,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 12),
                      child: Text(
                        'OK',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 14.sp,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );

//////////////////////////////////////////////////////////////////////////////

class TestScreen extends StatelessWidget {
  const TestScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Thanks_Card(
            context: context,
            onTap: const HomeScreen(),
          )
        ],
      ),
    );
  }
}
