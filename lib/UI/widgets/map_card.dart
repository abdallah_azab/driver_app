import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../Constast/Stayls/colors.dart';
import '../../Constast/image/images_app.dart';
import 'buttons/button_text.dart';
import 'text.dart';

// ignore: non_constant_identifier_names
Widget Map_Card({
  required String distance,
  required String time,
  required Function doneTap,
  required Function skipTap,
  required BuildContext context,
}) =>
    Center(
      child: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 8, right: 20, left: 20),
            child: Container(
              margin: const EdgeInsets.symmetric(vertical: 1, horizontal: 5),
              // width: 30.W,
              //    height: 16.h,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              child: Card(
                elevation: 3.5,
                shadowColor: Colors.black,
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Column(
                            children: [
                              Column(
                                children: [
                                  Text(
                                    '$distance ',
                                    style: TextStyle(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  MyText(
                                      title: 'Distance',
                                      color: ColorsApp.grey,
                                      size: 12.5.sp),
                                ],
                              ),
                              const SizedBox(height: 10),
                              ButtonWithTextApp(
                                hight: 5.5.h,
                                width: 25.w,
                                title: 'Done',
                                onTap: doneTap,
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Column(
                                children: [
                                  Text(
                                    '$time ',
                                    style: TextStyle(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  MyText(
                                      title: 'Arrival Time',
                                      color: ColorsApp.grey,
                                      size: 12.5.sp),
                                ],
                              ),
                              const SizedBox(height: 10),
                              ButtonWithTextApp(
                                hight: 5.5.h,
                                width: 25.w,
                                title: 'skip',
                                onTap: skipTap,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            right: -3,
            top: 3.h,
            child: const SizedBox(
              height: 70,
              width: 70,
              child: CircleAvatar(
                radius: 30.0,
                backgroundImage: AssetImage(Images.mapCardIcon),
                backgroundColor: Colors.transparent,
              ),
            ),
          ),
        ],
      ),
    );
