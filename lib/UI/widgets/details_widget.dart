import '../../Constast/Stayls/colors.dart';
import 'package:flutter/material.dart';

Widget textRow({String title1 = '', String title2 = '', colorTitle2}) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 2, vertical: 1),
    child: Row(
      children: [
        Text(
          title1.toString() + " : ",
          style: TextStyle(
              color: ColorsApp.defaultColor, fontWeight: FontWeight.w500),
        ),
        Text(
          title2.toString(),
          style: TextStyle(
              color: colorTitle2 ?? ColorsApp.defaultColor,
              fontWeight: FontWeight.w500),
        )
      ],
    ),
  );
}

Widget rowInfo(
    {String title1 = '',
    String supTitle1 = '',
    String title2 = '',
    String supTitle2 = ''}) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        textRow(title1: title1, title2: supTitle1),
        textRow(title1: title2, title2: supTitle2),
      ],
    ),
  );
}
