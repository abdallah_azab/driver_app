// ignore_for_file: must_be_immutable, prefer_typing_uninitialized_variables

import 'package:driver/Constast/Stayls/colors.dart';
import 'package:driver/Constast/image/images_app.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class AppBarCustom extends StatelessWidget {
  String title;
  bool showIcon;
  var icon;

  AppBarCustom(
      {Key? key, required this.title, this.showIcon = false, this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: showIcon == true
          ? const EdgeInsets.symmetric(vertical: 10)
          : const EdgeInsets.symmetric(vertical: 0),
      margin: const EdgeInsets.only(top: 12, bottom: 12, right: 8, left: 2),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: ColorsApp.offlight.withOpacity(0.6)),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            showIcon == true
                ? Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: Image.asset(
                      icon ?? Images.info,
                      height: 3.h,
                    ),
                  )
                : const SizedBox(),
            Text(
              title,
              style: TextStyle(
                  color: ColorsApp.defaultColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 15.sp),
            ),
          ],
        ),
      ),
    );
  }
}
