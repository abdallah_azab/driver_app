import 'UI/screens/splash/splash_screen.dart';
import 'provider/provider_info_me.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'provider/provider_routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:sizer/sizer.dart';

void main() {
  runApp(
    MultiProvider(
      providers: providers,
      child: const MyApp(),
    ),
  );
}

List<SingleChildWidget> providers = [
  ChangeNotifierProvider<RoutesProvider>(create: (_) => RoutesProvider()),
  ChangeNotifierProvider<InfoMeProvider>(create: (_) => InfoMeProvider()),
];

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return GetMaterialApp(
        theme: ThemeData(
          scaffoldBackgroundColor: Colors.white,
        ),
        debugShowCheckedModeBanner: false,
        title: 'Driver App',
        // home: isLogin == true ? const HomePage() : const LoginScreen(),
        // home:  const LoginScreen(),
        home: const SplashScreen(),
      );
    });
  }

  bool? isLogin;

  Future<bool> getIsLogin() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      isLogin = preferences.getBool('ISLOGIN');
    });
    debugPrint(":::: isLogin ::::" + isLogin.toString());
    return isLogin ?? false;
  }

  @override
  void initState() {
    super.initState();
    // getIsLogin();
  }
}
