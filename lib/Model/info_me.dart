/*
class InfoMeModel {
  InfoMeModel({
    required this.data,
  });

  late final Data data;

  InfoMeModel.fromJson(Map<String, dynamic> json) {
    data = Data.fromJson(json['data']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['data'] = data.toJson();
    return _data;
  }
}

class Data {
  Data({
    required this.id,
    required this.name,
    required this.email,
    required this.admin,
    required this.manager,
    required this.driver,
    this.companyId,
    required this.createdAt,
    required this.updatedAt,
  });

  late final int id;
  late final String name;
  late final String email;
  late final int admin;
  late final int manager;
  late final int driver;
  // ignore: prefer_void_to_null
  late final Null companyId;
  late final String createdAt;
  late final String updatedAt;

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    admin = json['admin'];
    manager = json['manager'];
    driver = json['driver'];
    companyId = null;
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['name'] = name;
    _data['email'] = email;
    _data['admin'] = admin;
    _data['manager'] = manager;
    _data['driver'] = driver;
    _data['company_id'] = companyId;
    _data['created_at'] = createdAt;
    _data['updated_at'] = updatedAt;
    return _data;
  }
}

*/



class InfoMeModel {
  InfoMeModel({
    required this.data,
  });
  late final Data data;

  InfoMeModel.fromJson(Map<String, dynamic> json){
    data = Data.fromJson(json['data']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['data'] = data.toJson();
    return _data;
  }
}

class Data {
  Data({
    required this.id,
    required this.name,
    required this.email,
    required this.disabled,
    required this.expiryDate,
    this.carType,
    this.carId,
    this.personId,
    required this.admin,
    required this.manager,
    required this.driver,
    this.companyId,
    required this.createdAt,
    required this.updatedAt,
  });
  late final int id;
  late final String name;
  late final String email;
  late final int disabled;
  late final String expiryDate;
  var carType;
  var carId;
  var personId;
  late final int admin;
  late final int manager;
  late final int driver;
  var companyId;
  late final String createdAt;
  late final String updatedAt;

  Data.fromJson(Map<String, dynamic> json){
    id = json['id'];
    name = json['name'];
    email = json['email'];
    disabled = json['disabled'];
    expiryDate = json['expiry_date'];
    carType = null;
    carId = null;
    personId = null;
    admin = json['admin'];
    manager = json['manager'];
    driver = json['driver'];
    companyId = null;
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['name'] = name;
    _data['email'] = email;
    _data['disabled'] = disabled;
    _data['expiry_date'] = expiryDate;
    _data['car_type'] = carType;
    _data['car_id'] = carId;
    _data['person_id'] = personId;
    _data['admin'] = admin;
    _data['manager'] = manager;
    _data['driver'] = driver;
    _data['company_id'] = companyId;
    _data['created_at'] = createdAt;
    _data['updated_at'] = updatedAt;
    return _data;
  }
}