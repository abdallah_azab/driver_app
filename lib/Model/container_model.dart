class ContainersModel {
  ContainersModel({
    required this.data,
  });

  late final List<Data> data;

  ContainersModel.fromJson(Map<String, dynamic> json) {
    data = List.from(json['data']).map((e) => Data.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['data'] = data.map((e) => e.toJson()).toList();
    return _data;
  }
}

class Data {
  Data({
    required this.id,
    required this.name,
    required this.description,
    required this.companyId,
    required this.disabled,
    required this.startTime,
    required this.endTime,
    required this.createdAt,
    required this.updatedAt,
    required this.points,
  });

  late final int id;
  late final String name;
  late final String description;
  late final int companyId;
  late final int disabled;
  late final String startTime;
  late final String endTime;
  late final String createdAt;
  late final String updatedAt;
  late final List<Points> points;

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    companyId = json['company_id'];
    disabled = json['disabled'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    points = List.from(json['points']).map((e) => Points.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['name'] = name;
    _data['description'] = description;
    _data['company_id'] = companyId;
    _data['disabled'] = disabled;
    _data['start_time'] = startTime;
    _data['end_time'] = endTime;
    _data['created_at'] = createdAt;
    _data['updated_at'] = updatedAt;
    _data['points'] = points.map((e) => e.toJson()).toList();
    return _data;
  }
}

class Points {
  Points({
    required this.id,
    required this.name,
    required this.type,
    required this.latitude,
    required this.longitude,
    required this.routeId,
    required this.sort,
    required this.createdAt,
    required this.updatedAt,
  });

  late final int id;
  late final String name;
  late final String type;
  late final double latitude;
  late final double longitude;
  late final int routeId;
  late final int sort;
  late final String createdAt;
  late final String updatedAt;

  Points.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    type = json['type'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    routeId = json['route_id'];
    sort = json['sort'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['name'] = name;
    _data['type'] = type;
    _data['latitude'] = latitude;
    _data['longitude'] = longitude;
    _data['route_id'] = routeId;
    _data['sort'] = sort;
    _data['created_at'] = createdAt;
    _data['updated_at'] = updatedAt;
    return _data;
  }
}
