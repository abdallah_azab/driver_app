import 'package:flutter/material.dart';

// class HelperAction {
// =-=- to show snack bar custom =-=-=-=-
showSnackBarCustom({required context, text}) {
  final snackBar = SnackBar(
    content: Text(text ?? ""),
    backgroundColor: Colors.green,
    behavior: SnackBarBehavior.floating,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(15),
    ),
    elevation: 6.5,
  );

  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}
