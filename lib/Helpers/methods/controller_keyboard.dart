import 'package:flutter/material.dart';

// Hide the soft keyboard.
void hideKeyboard(BuildContext context) {
  FocusScope.of(context).requestFocus(FocusNode());
}

void showKeyboard(BuildContext context) {
  FocusScope.of(context).requestFocus();
}

// Hide the soft keyboard.
void dismissKeyboard(BuildContext context) {
  FocusScope.of(context).unfocus();
}
