import 'package:flutter/cupertino.dart';
import 'package:location/location.dart';

class HelpLMap {
  static Future<LocationData> getCurrentLocation() async {
    Location location = Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        debugPrint('::::: _serviceEnabled ::::: ' + _serviceEnabled.toString());
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        debugPrint(
            '::::: _permissionGranted ::::: ' + _permissionGranted.toString());
      }
    }

    _locationData = await location.getLocation();

    return _locationData;
  }
}
