import '../../Constast/Stayls/colors.dart';
import '../../UI/widgets/text.dart';
import 'package:flutter/material.dart';

class PopScopeApp extends StatelessWidget {
  // ignore: prefer_typing_uninitialized_variables
  final child;

  const PopScopeApp({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _onWillPop(context),
      child: child,
    );
  }

  Future<bool> _onWillPop(context) async {
    return (await showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: MyText(title: ''),
            content: MyText(title: 'Are you sure to exit the App ?'),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: MyText(title: 'No', color: ColorsApp.lightOrange),
              ),
              TextButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: MyText(title: 'yes', color: ColorsApp.green),
              ),
            ],
          ),
        )) ??
        false;
  }
}
