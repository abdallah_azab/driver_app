import 'package:flutter/material.dart';

navigatorPush({required BuildContext context, required page}) {
  Navigator.push(context, MaterialPageRoute(builder: (context) => page));
}

navigator({required BuildContext context, required page}) {
  Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => page,
      ));
}
